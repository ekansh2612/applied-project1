//
//  PaymentViewController.swift
//  menu
//
//  Created by Ekansh Sharma on 2019-09-25.
//  Copyright © 2019 lambton. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavBar()
    }
    
    func configureNavBar() {
        navigationItem.title = "PAYMENT"
    }
}
